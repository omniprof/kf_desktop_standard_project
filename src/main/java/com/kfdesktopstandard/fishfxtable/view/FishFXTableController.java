package com.kfdesktopstandard.fishfxtable.view;

import java.sql.SQLException;

import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert;
import javafx.application.Platform;
import javafx.event.ActionEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kfdesktopstandard.fishfxtable.beans.FishData;
import com.kfdesktopstandard.fishfxtable.persistence.FishDAO;

import java.util.ResourceBundle;
import javafx.stage.Modality;

/**
 * This is the controller class for the FishFXTableLayout.fxml
 *
 * @author Ken Fogel
 * @version 2.0
 */
public class FishFXTableController {

    private final Logger log = LoggerFactory.getLogger(this.getClass()
            .getName());

    // Database access
    private FishDAO fishDAO;
    private FishData fishData;

    // Resource bundle is injected when controller is loaded
    @FXML
    private ResourceBundle resources;

    // All of the following are objcets defined in the fxml file that will be
    // injected into these variables by the JavaFX framework
    @FXML
    private TableView<FishData> fishDataTable;

    @FXML
    private TableColumn<FishData, Number> idColumn;

    @FXML
    private TableColumn<FishData, String> commonNameColumn;

    @FXML
    private TableColumn<FishData, String> latinColumn;

    @FXML
    private TableColumn<FishData, String> phColumn;

    @FXML
    private TableColumn<FishData, String> khColumn;

    @FXML
    private TableColumn<FishData, String> tempColumn;

    @FXML
    private TableColumn<FishData, String> fishSizeColumn;

    @FXML
    private TableColumn<FishData, String> speciesOriginColumn;

    @FXML
    private TableColumn<FishData, String> tankSizeColumn;

    @FXML
    private TableColumn<FishData, String> stockingColumn;

    @FXML
    private TableColumn<FishData, String> dietColumn;

    /**
     * The framework creates this object so is is not possible to pass anything
     * to this class through this constructor.
     */
    public FishFXTableController() {
        super();
        log.info("Default Constructor");
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {

        // Connects the property in the FishData object to the column in the table
        idColumn.setCellValueFactory(cellData -> cellData.getValue()
                .idProperty());
        commonNameColumn.setCellValueFactory(cellData -> cellData.getValue()
                .commonNameProperty());
        latinColumn.setCellValueFactory(cellData -> cellData.getValue()
                .latinProperty());
        phColumn.setCellValueFactory(cellData -> cellData.getValue()
                .phProperty());
        khColumn.setCellValueFactory(cellData -> cellData.getValue()
                .khProperty());
        tempColumn.setCellValueFactory(cellData -> cellData.getValue()
                .tempProperty());
        fishSizeColumn.setCellValueFactory(cellData -> cellData.getValue()
                .fishSizeProperty());
        speciesOriginColumn.setCellValueFactory(cellData -> cellData.getValue()
                .speciesOriginProperty());
        tankSizeColumn.setCellValueFactory(cellData -> cellData.getValue()
                .tankSizeProperty());
        stockingColumn.setCellValueFactory(cellData -> cellData.getValue()
                .stockingProperty());
        dietColumn.setCellValueFactory(cellData -> cellData.getValue()
                .dietProperty());

        // Set the column widths of the table
        adjustColumnWidths();

        // Listen for selection changes and show the selected fishData details
        // when a new row is selected.
        fishDataTable
                .getSelectionModel()
                .selectedItemProperty()
                .addListener(
                        (observable, oldValue, newValue) -> showFishDetails(newValue));
    }

    /**
     * Event handle when the Close choice in the File menu is selected
     *
     * @param event
     */
    @FXML
    private void handleClose(ActionEvent event) {
        Platform.exit();
    }

    /**
     * Sets a reference to the FishDAO object that retrieves data from the
     * database.
     *
     * @param fishDAO
     * @throws SQLException
     */
    public void setFishDataDAO(FishData fishData, FishDAO fishDAO) throws SQLException {
        this.fishDAO = fishDAO;
    }

    /**
     * This adds the observable list to the table. It must occur after the
     * reference to the DAO is provided.
     *
     * @throws SQLException
     */
    public void displayTheTable() throws SQLException {
        // Add observable list data to the table
        fishDataTable.setItems(this.fishDAO.findAll());
    }

    /**
     * Sets the width of the columns based on a percentage of the overall width
     * if the fishDataTable. This width has been set in the fxml file.
     */
    private void adjustColumnWidths() {
        // Get the current width of the table
        double width = fishDataTable.getPrefWidth();
        // Set width of each column
        idColumn.setPrefWidth(width * .05);
        commonNameColumn.setPrefWidth(width * .15);
        latinColumn.setPrefWidth(width * .15);
        phColumn.setPrefWidth(width * .05);
        khColumn.setPrefWidth(width * .05);
        tempColumn.setPrefWidth(width * .05);
        fishSizeColumn.setPrefWidth(width * .1);
        speciesOriginColumn.setPrefWidth(width * .1);
        tankSizeColumn.setPrefWidth(width * .1);
        stockingColumn.setPrefWidth(width * .1);
        dietColumn.setPrefWidth(width * .1);
    }

    /**
     * To be able to test the selection handler for the table, this method
     * displays the FishData object that corresponds to the selected row. This
     * dialog is configured to be non-modal so it does not block. It does not
     * have to be closed and more than one dialog can open.
     *
     * @param fishData
     */
    private void showFishDetails(FishData fishData) {
        Alert dialog = new Alert(Alert.AlertType.INFORMATION);
        // Non-modal setting
        dialog.initModality(Modality.NONE);
        dialog.setTitle(resources.getString("FishDetails"));
        dialog.setHeaderText(resources.getString("FishDetails"));
        dialog.setContentText(fishData.toString());
        dialog.show();

    }

    /**
     * Event handle when the About choice in the Help menu is selected. By
     * default Alert dialogs are modal and are blocking. It must be closed.
     *
     * @param event
     */
    @FXML
    private void handleAbout(ActionEvent event) {
        Alert dialog = new Alert(Alert.AlertType.INFORMATION);
        dialog.setTitle(resources.getString("AboutTitle"));
        dialog.setHeaderText(resources.getString("AboutTitle"));
        dialog.setContentText(resources.getString("AboutText"));
        dialog.show();
    }
}
