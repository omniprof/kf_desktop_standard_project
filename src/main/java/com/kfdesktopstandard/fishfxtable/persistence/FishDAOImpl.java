/**
 * This class provides the functionality to: <ul> <li>1) Open a database <li>2)
 * Retrieve all the records and return them as a JavaFX ObservableList
 * <li>3) Close the database </ul>
 *
 * @author Ken Fogel
 * @version 2.0
 */
package com.kfdesktopstandard.fishfxtable.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import com.kfdesktopstandard.fishfxtable.beans.FishData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements the FishDAO interface
 *
 * Exceptions are possible whenever a JDBC object is used. When these exceptions
 * occur it should result in some message appearing to the user and possibly
 * some corrective action. To simplify this, all exceptions are thrown and not
 * caught here. The methods that you write to call any of these methods must
 * either use a try/catch or continue throwing the exception.
 *
 * @author Ken Fogel
 * @version 2.0
 *
 */
public class FishDAOImpl implements FishDAO {

    private final Logger log = LoggerFactory.getLogger(this.getClass()
            .getName());

    private final String url ="jdbc:mysql://localhost:3306/Aquarium?characterEncoding=UTF-8&autoReconnect=true&zeroDateTimeBehavior=CONVERT_TO_NULL&useSSL=false&allowPublicKeyRetrieval=true";

    // There is no secure way to store a username and password in an application.
    // If security is important then the application should use a mechanism where
    // the user supplies the credentials rather than coding them as below.
    private final String user = "fish";
    private final String password = "kfstandard";

    public FishDAOImpl() {
        super();
    }

    /**
     * Read
     *
     * Retrieve all the records for the given table and returns the data as an
     * ObservableList of FishData objects
     *
     * @return The ObservableList of FishData objects
     * @throws java.sql.SQLException
     */
    @Override
    public ObservableList<FishData> findAll() throws SQLException {

        ObservableList<FishData> fishData = FXCollections
                .observableArrayList();

        String selectQuery = "SELECT ID, COMMONNAME, LATIN, PH, KH, TEMP, FISHSIZE, SPECIESORIGIN, TANKSIZE, STOCKING, DIET FROM FISH";

        // Using try with resources. Classes that implement the Closable
        // interface created in the try's parenthesis () will be closed when the
        // try block ends for any reason
        try (Connection connection = DriverManager.getConnection(url, user,
                password);
                // You must use PreparedStatements to guard against SQL Injection
                PreparedStatement pStatement = connection
                        .prepareStatement(selectQuery);
                ResultSet resultSet = pStatement.executeQuery()) {
            while (resultSet.next()) {
                fishData.add(convertResultSetToFishData(resultSet));
            }
        }
        log.info("# of records found : " + fishData.size());
        return fishData;
    }


    /*
    Basic CRUD Methods
     */
    /**
     * Read
     *
     * Retrieve one record from the given table based on the primary key
     *
     * @param id
     * @return The FishData object
     * @throws java.sql.SQLException
     */
    @Override
    public FishData findID(int id) throws SQLException {

        // If there is no record with the desired id then this will be returned
        // as a null pointer
        FishData fishData = null;

        String selectQuery = "SELECT ID, COMMONNAME, LATIN, PH, KH, TEMP, FISHSIZE, SPECIESORIGIN, TANKSIZE, STOCKING, DIET FROM FISH WHERE ID = ?";

        // Using try with resources. Classes that implement the Closable
        // interface created in the try's parenthesis () will be closed when the
        // try block ends for any reason
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                PreparedStatement pStatement = connection
                        .prepareStatement(selectQuery);) {
            // Only object creation statements can be in the parenthesis so
            // first try-with-resources block ends
            pStatement.setInt(1, id);
            // A new try-with-resources block for creating the ResultSet object
            // begins
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    fishData = convertResultSetToFishData(resultSet);
                }
            }
        }
        log.info("Found " + id + "?: " + (fishData != null));
        return fishData;
    }

    /**
     * Create
     *
     * This method adds a FishData object as a record to the database. The
     * column list does not include ID as this is assigned by the database.
     *
     * @param fishData
     * @return The number of records created, should always be 1
     * @throws SQLException
     */
    @Override
    public int create(FishData fishData) throws SQLException {

        int result;
        String createQuery = "INSERT INTO FISH (COMMONNAME, LATIN, PH, KH, TEMP, FISHSIZE, SPECIESORIGIN, TANKSIZE, STOCKING, DIET) VALUES (?,?,?,?,?,?,?,?,?,?)";

        // Using try with resources. Classes that implement the Closable
        // interface created in the try's parenthesis () will be closed when the
        // try block ends for any reason
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                PreparedStatement ps = connection.prepareStatement(createQuery);) {
            ps.setString(1, fishData.getCommonName());
            ps.setString(2, fishData.getLatin());
            ps.setString(3, fishData.getPh());
            ps.setString(4, fishData.getKh());
            ps.setString(5, fishData.getTemp());
            ps.setString(6, fishData.getFishSize());
            ps.setString(7, fishData.getSpeciesOrigin());
            ps.setString(8, fishData.getTankSize());
            ps.setString(9, fishData.getStocking());
            ps.setString(10, fishData.getDiet());

            result = ps.executeUpdate();
        }
        log.info("# of records created : " + result);
        return result;
    }

    /**
     * Delete
     *
     * This method deletes a single record based on the criteria of the primary
     * key field ID value. It should return either 0 meaning that there is no
     * record with that ID or 1 meaning a single record was deleted. If the
     * value is greater than 1 then something unexpected has happened. A
     * criteria other than ID may delete more than one record.
     *
     * @param id
     * @par @return The number of records deleted, should be 0 or 1
     * @throws SQLException
     */
    @Override
    public int delete(int id) throws SQLException {

        int result;

        String deleteQuery = "DELETE FROM FISH WHERE ID = ?";

        // Using try with resources. Classes that implement the Closable
        // interface created in the try's parenthesis () will be closed when the
        // try block ends for any reason
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                PreparedStatement ps = connection.prepareStatement(deleteQuery);) {
            ps.setInt(1, id);
            result = ps.executeUpdate();
        }
        log.info("# of records deleted : " + result);
        return result;
    }

    /**
     * Update
     *
     * This method will update all the fields of a record except ID. Usually
     * updates are tied to specific fields and so only those fields need appear
     * in the SQL statement.
     *
     * @param fishData An object with an existing ID and new data in the fields
     * @return The number of records updated, should be 0 or 1
     * @throws SQLException
     *
     */
    @Override
    public int update(FishData fishData) throws SQLException {

        int result;

        String updateQuery = "UPDATE FISH SET COMMONNAME=?, LATIN=?, PH=?, KH=?, TEMP=?, FISHSIZE=?, SPECIESORIGIN=?, TANKSIZE=?, STOCKING=?, DIET=? WHERE ID = ?";

        // Using try with resources. Classes that implement the Closable
        // interface created in the try's parenthesis () will be closed when the
        // try block ends for any reason
        try (Connection connection = DriverManager.getConnection(url, user, password);
                // You must use a prepared statement to handle the conversion
                // of special characters in the SQL statement and guard against
                // SQL Injection
                PreparedStatement ps = connection.prepareStatement(updateQuery);) {
            ps.setString(1, fishData.getCommonName());
            ps.setString(2, fishData.getLatin());
            ps.setString(3, fishData.getPh());
            ps.setString(4, fishData.getKh());
            ps.setString(5, fishData.getTemp());
            ps.setString(6, fishData.getFishSize());
            ps.setString(7, fishData.getSpeciesOrigin());
            ps.setString(8, fishData.getTankSize());
            ps.setString(9, fishData.getStocking());
            ps.setString(10, fishData.getDiet());

            result = ps.executeUpdate();
        }
        log.info("# of records updated : " + result);
        return result;
    }

    /**
     * Converts the ResultSet to a FishData object
     *
     * @param resultSet
     * @return the FishData object
     * @throws SQLException
     */
    private FishData convertResultSetToFishData(ResultSet resultSet) throws SQLException {
        FishData fishData = new FishData();
        fishData.setCommonName(resultSet.getString("COMMONNAME"));
        fishData.setDiet(resultSet.getString("DIET"));
        fishData.setKh(resultSet.getString("KH"));
        fishData.setLatin(resultSet.getString("LATIN"));
        fishData.setPh(resultSet.getString("PH"));
        fishData.setFishSize(resultSet.getString("FISHSIZE"));
        fishData.setSpeciesOrigin(resultSet.getString("SPECIESORIGIN"));
        fishData.setStocking(resultSet.getString("STOCKING"));
        fishData.setTankSize(resultSet.getString("TANKSIZE"));
        fishData.setTemp(resultSet.getString("TEMP"));
        fishData.setId(resultSet.getInt("ID"));
        return fishData;
    }

}
