package com.kfdesktopstandard.fishfxtable.persistence;

import com.kfdesktopstandard.fishfxtable.beans.FishData;
import java.sql.SQLException;
import javafx.collections.ObservableList;

/**
 * Interface for CRUD methods
 *
 * @author Ken Fogel
 * @version 2.0
 */
public interface FishDAO {

    public ObservableList<FishData> findAll() throws SQLException;

    // Create
    public int create(FishData fishData) throws SQLException;

    // Read
    public FishData findID(int id) throws SQLException;

    // Update
    public int update(FishData fishData) throws SQLException;

    // Delete
    public int delete(int ID) throws SQLException;
}
